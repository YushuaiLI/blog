FROM ruby:2.5.9
RUN apt-get update -qq \
    && apt-get install -y nodejs sqlite3 libsqlite3-dev npm && npm install --global yarn
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN gem install rails && bundle install
COPY . /myapp
RUN rails db:migrate && rails webpacker:install
CMD ["rails", "server", "-b", "0.0.0.0"]